using System.Threading;
using System;
using Xunit;
using UnderTestLibrary;
using System.Collections.Generic;

namespace GenericLibraryTests
{
    
    public class UnitTest1
    {
        [Fact]
        public void ItShouldAlertIfOneMonitorIsOver90()
        {
            //ARRANGE
                var listOfMonitors = new List<HeatMonitor>();
                listOfMonitors.Add(new HeatMonitor( 99));
            //ACT
                var sut = new AlertCentral(listOfMonitors);
            //ASSERT
                Assert.Equal(AlertCentralStatus.OverHeat, sut.GlobalStatus);
        }

     [Fact]
        public void ItShouldAlertIfOnlyOneMonitorIsOver90OutOfTwo()
        {
            //ARRANGE
                var listOfMonitors = new List<HeatMonitor>();
                 listOfMonitors.Add(new HeatMonitor( 9));
                  listOfMonitors.Add(new HeatMonitor( 99));
            //ACT
                var sut = new AlertCentral(listOfMonitors);
            //ASSERT
                Assert.Equal(AlertCentralStatus.OverHeat, sut.GlobalStatus);
        }    
    }
}
 