namespace UnderTestLibrary
{
    public class HeatMonitor
    {
        
        private int temperature;

        public HeatMonitor(int v)
        {
            this.temperature = v;
        }

        public int Temperature { get => temperature;  }
    }
}